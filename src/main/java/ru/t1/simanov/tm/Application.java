package ru.t1.simanov.tm;

import ru.t1.simanov.tm.component.Bootstrap;

public final class Application {

    public static void main(final String[] args) {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}
